<?php

// Inclui arquivo com funções úteis
require_once( 'functions.php' );

// Verifica se existe um arquivo de configuração na pasta /inc
if ( file_exists( "inc/config.php" ) ) {

	require_once( 'inc/config.php' );

} else {

	// Banco de dados (mover para um arquivo externo)

	// Credenciais
	define( 'DB_HOST', 'localhost' );
	define( 'DB_USER', 'root' );
	define( 'DB_PASS', '' );
	define( 'DB_NAME', 'engenhoearte_bd' );

}

// $conn é a variável da conexão com o banco de dados
$conn = new mysqli( DB_HOST, DB_USER, DB_PASS );

// Verifica a conexão com o banco de dados
if ( $conn->connect_error ){
	echo "Conexão não detectada: " . $conn->connect_error;
}

// Seleciona o banco de dados definido em DB_NAME
$conn->select_db( DB_NAME );
	
$btnLogin = filter_input( INPUT_POST, 'btnLogin', FILTER_SANITIZE_STRING );

if ( $btnLogin ) {

	$email	= filter_input( INPUT_POST, 'email', FILTER_SANITIZE_STRING );
	//$senha	= filter_input( INPUT_POST, 'senha', FILTER_SANITIZE_STRING );
	$senha = $_POST['senha'];

	if ( ! empty( $email ) && ! empty( $senha ) ) {
		
		// Pesquisar o usuário no BD usando como referência o e-mail
		$sql = "SELECT * FROM usuarios WHERE email = '$email' LIMIT 1";
		$result	= $conn->query( $sql );

		// Verifica se o usuário existe
		if ( $result->num_rows == 1 ) {

			while ( $row = $result->fetch_object() ){
		        $user_arr[] = $row;
		    }

		    $result->close();

		    // Verifica se a senha está correta
		    if ( md5( $senha ) == $user_arr[0]->password ) {
				$_SESSION['ID']		= $user_arr[0]->ID;
				$_SESSION['nome']	= $user_arr[0]->nome;
				$_SESSION['email']	= $user_arr[0]->email;
		    	header( "Location: portfolio.php" );
		    } else {
		    	$_SESSION['msg'] = "Dados incorretos, tente novamente!";
		    	header( "Location: login.php" );
		    }

		} else {
			// O usuário não existe
			$_SESSION['msg'] = "Dados não encontrados no banco de dados!";
			header( "Location: login.php" );
		}
	}

} else {
	$_SESSION['msg'] = "Página não encontrada!";
	header( "Location: login.php" );
}

?>