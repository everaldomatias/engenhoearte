<nav class="menu">
	
	<?php generate_menu_item( 'index', 'Home' ); ?>
	<?php if ( ! empty( $_SESSION['ID'] ) ) : ?>
		<?php generate_menu_item( 'portfolio', 'Portfólio' ); ?>
	<?php endif; ?>
	<?php generate_menu_item( 'sobre-nos', 'Sobre Nós' ); ?>
	<?php generate_menu_item( 'contato', 'Contato' ); ?>
	<?php generate_menu_item( 'cadastro', 'Cadastro' ); ?>
	<?php if ( empty( $_SESSION['ID'] ) ) : ?>
		<?php generate_menu_item( 'login', 'Login' ); ?>
	<?php endif; ?>

</nav>