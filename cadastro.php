<?php

// Inclui arquivo com funções úteis
require_once( 'functions.php' );

if ( file_exists( "inc/config.php" ) ) {

	require_once( 'inc/config.php' );

} else {

	// Banco de dados (mover para um arquivo externo)

	// Credenciais
	define( 'DB_HOST', 'localhost' );
	define( 'DB_USER', 'root' );
	define( 'DB_PASS', '' );
	define( 'DB_NAME', 'engenhoearte_bd' );

}

// $conn é a variável da conexão com o banco de dados
$conn = new mysqli( DB_HOST, DB_USER, DB_PASS );

// Verifica a conexão com o banco de dados
if ( $conn->connect_error ){
	echo "Conexão não detectada: " . $conn->connect_error;
}

// Cria o banco de dados 'engenhoearte_bd' se ele ainda não existir
$database_sql = "CREATE DATABASE IF NOT EXISTS " . DB_NAME;
if ( $conn->query( $database_sql ) === FALSE ) {
	return true;
}

// Seleciona o banco de dados definido em DB_NAME
$conn->select_db( DB_NAME );

// Cria a tabela 'usuarios' se ela ainda não existir
$table_sql = "
	CREATE TABLE IF NOT EXISTS " . DB_NAME . ".usuarios(
	ID INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
	nome VARCHAR(255) NOT NULL,
	email VARCHAR(255) NOT NULL,
	password VARCHAR(255) NOT NULL
	)";

if ( $conn->query( $table_sql ) === FALSE ){
	echo "A tabela não foi criada: " . $conn->error;
}


// Inicializa as variáveis que receberão as informações do formulário
$nome =		'';
$email =	'';
$senha =	'';


if ( isset( $_POST['submit'] ) ) {

	// Nome
	if ( $_POST['nome'] != '' ) {
		$nome = filter_var( $_POST['nome'], FILTER_SANITIZE_STRING );
		$nameError = "<span class='valid'>" . $nome . "</span> is Sanitized an Valid name.";
		if ( $_POST['nome'] == '' ) {
			$nameError = "<span class='invalid'>Please enter a valid name.</span>";
		}
	} else {
		$nameError = "<span class='invalid'>Por favor di.</span>";
	}

	// E-mail
	if ( $_POST['email'] != '' ) {
		$email = filter_var( $_POST['email'], FILTER_SANITIZE_EMAIL );
		$email = filter_var( $email, FILTER_VALIDATE_EMAIL );
		$emailError = "<span class='valid'>" . $email . " </span>is Sanitized an Valid Email.";
		if ( $email == '' ) {
			$emailError = "<span class='invalid'>Please enter a valid email.</span>";
		}
	} else {
		$emailError = "<span class='invalid'>Please enter your email.</span>";
	}

	// Senha
	if ( $_POST['senha'] != '' ) {
		$senha = $_POST['senha'];
	}

	if ( ! empty( $nome ) && ! empty( $email ) && ! empty( $senha ) ) {

		$sql = "SELECT * FROM usuarios WHERE nome = '$nome' AND email = '$email'";
		$result	= $conn->query( $sql );

		// Verifica se o registro existe
		if ( $result->num_rows > 0 ) {

		    $rows_count = $result->num_rows;
		    $result->close();

		    $erro = "Esse usuário não pode ser criado.";
		    $status = 'warning';

		} else {

			// Se não, insere na tabela
			$insert = "INSERT INTO usuarios ( nome, email, password ) VALUES ( '$nome', '$email', MD5('$senha') )";
			if ( $conn->query( $insert ) === TRUE ) {
			    $erro = 'Usuário criado com sucesso!';
			    $status = 'success';
			} else {
			    $erro = "Error: " . $insert . "<br>" . $conn->error;
			    $status = 'warning';
			}

		}
	}

}

?>

<?php require_once( 'header.php' ); ?>

<body class="cadastro">
	<script src="scripts.js"></script>

	<header>
		<div class="container">
			<a href="index.php" title="Voltar para Home">
				<img class="logo" src="imagens/logo.png" alt="Home - Engenho & Arte WebDesign Studio LTDA">				
			</a>
			<?php show_logged_name(); ?>
			<?php require_once( 'menu.php' ); ?>
		</div>
	</header>

	<section class="conteudo">
		
		<h1>Cadastro de usuário</h1>
		<p>Crie sua conta em nosso site e em breve teremos conteúdos exclusivos para você!</p>

		<?php if ( isset( $_POST['submit'] ) && isset( $erro ) && isset( $status ) ) : ?>
			<div class="container alert alert-<?php echo $status; ?>">
				<span><?php echo $erro; ?></span>
			</div>
		<?php endif; ?>

		<form action="cadastro.php" method="post" id="validaAcesso">
			<label>Nome:</label>
			<input type="text" id="nome" name="nome" placeholder="Seu nome completo" required>

			<label>Email:</label>
			<input type="email" id="email" name="email" placeholder="Precisamos do seu e-mail para contatos futuros" required>

			<label>Senha:</label>
			<input type="password" id="senha" name="senha" placeholder="Sua senha ultra secreta e que só você sabe" required>

			<input type="submit" name="submit" value="Efetuar Cadastro" />
		</form>		

	</section><!-- conteudo -->

	<footer>
		<div class="container">
			<img class="logo" src="imagens/logo.png" alt="Contato - Engenho & Arte WebDesign Studio LTDA">
			<div class="infos">
				<p>
					<b>Enhenho & Arte</b><br>
					Rua Paulo Faccini, nº 318, Bosque Maia, SP<br>
					CEP 00000-000
				</p>
			</div><!-- infos -->
			<div class="mapa">
				<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3077.7645532813835!2d-46.532713193782264!3d-23.45687497305292!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94cef5409c541c25%3A0x3d75ed9f7dc0aa02!2sAv.+Paulo+Faccini%2C+318+-+Macedo%2C+Guarulhos+-+SP!5e0!3m2!1spt-BR!2sbr!4v1510613939243" width="510" height="200" frameborder="0" style="border:0" allowfullscreen></iframe>
			</div><!-- mapa -->
		</div><!-- container -->
	</footer>
</body>
</html>