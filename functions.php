<?php

session_start();

function trim_value( &$value ) {
	// Remove espaços em branco e caracteres relacionados do início e do fim da string ($value)
    $value = trim( $value );
}

function get_current_page() {
	$url = $_SERVER["REQUEST_URI"];
	$path = parse_url( $url, PHP_URL_PATH );
	return basename( $path, ".php" );
}

function generate_menu_item( $link, $name ) {
	$current_page = get_current_page();
	if ( $current_page == $link ) {
		$class = 'ativo';
	} else {
		$class = 'default';
	}
	echo '<a class="' . $class . '" href="' . $link . '.php" title="Você está na ' . $name . '">' . $name . '</a>';
}

function show_logged_name() {
	if ( ! empty( $_SESSION['ID'] )  ) {
		echo '<span class="exibe-nome">';
		echo "Olá <strong>" . $_SESSION['nome'] . "</strong>, seja bem vind@! ";
		echo "<a href='sair.php'> [sair]</a>";
		echo "</span>";
	}
}