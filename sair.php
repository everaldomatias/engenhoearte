<?php

session_start();

unset( $_SESSION['ID'], $_SESSION['nome'], $_SESSION['email'] );
$_SESSION['msg'] = "Deslogado com sucesso! Até breve!";
header( "Location: login.php" );
exit;