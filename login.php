<?php

// Inclui arquivo com funções úteis
include_once( 'functions.php' );

?>

<?php require_once( 'header.php' ); ?>

<body class="login">
	<script src="scripts.js"></script>

	<header>
		<div class="container">
			<a href="index.php" title="Voltar para Home">
				<img class="logo" src="imagens/logo.png" alt="Home - Engenho & Arte WebDesign Studio LTDA">				
			</a>
			<?php show_logged_name(); ?>
			<?php require_once( 'menu.php' ); ?>
		</div>
	</header>

	<section class="conteudo">
		
		<h1>Login</h1>
		<p>Acesso a área restrita do site e tenha acesso a conteúdos exclusivos.</p>

		<?php if ( isset( $_SESSION['msg'] ) ) : ?>
	  		<?php
	  			$erro = $_SESSION['msg'];
	  			unset( $_SESSION['msg'] );
	  		?>
	  		<div class="container alert alert-warning">
				<span><?php echo $erro; ?></span>
			</div>
	  	<?php endif; ?>

	  	<form action="valida.php" method="post" id="Login">
			<label>Usuário:</label>
			<input type="text" id="email" name="email" placeholder="Digite o seu e-mail">
			<label>Senha:</label>
			<input type="password" id="senha" name="senha" placeholder="Digite a sua senha">

			<input type="submit" name="btnLogin" value="Acessar" />
		</form>

	</section><!-- conteudo -->

	<footer>
		<div class="container">
			<img class="logo" src="imagens/logo.png" alt="Contato - Engenho & Arte WebDesign Studio LTDA">
			<div class="infos">
				<p>
					<b>Enhenho & Arte</b><br>
					Rua Paulo Faccini, nº 318, Bosque Maia, SP<br>
					CEP 00000-000
				</p>
			</div><!-- infos -->
			<div class="mapa">
				<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3077.7645532813835!2d-46.532713193782264!3d-23.45687497305292!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94cef5409c541c25%3A0x3d75ed9f7dc0aa02!2sAv.+Paulo+Faccini%2C+318+-+Macedo%2C+Guarulhos+-+SP!5e0!3m2!1spt-BR!2sbr!4v1510613939243" width="510" height="200" frameborder="0" style="border:0" allowfullscreen></iframe>
			</div><!-- mapa -->
		</div><!-- container -->
	</footer>
</body>
</html>