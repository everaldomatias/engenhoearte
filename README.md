# Engenho & Arte Webdesign Studio

### Site da empresa Engenho & Arte Webdesign Studio

### Dependências

* PHP > 5.6
* MySQL > 5.5

### Tecnologia

* PHP
* SQL
* HTML
* CSS

### Instalação

Dentro da pasta inc/ existe o arquivo config-sample.php, faça uma cópia dele nessa mesma pasta com o nome de config.php
Nesse arquivo você precisará definir os dados para conexão com um banco de dados (MySQL) previamente existente.

Com a conexão o próprio sistema criará a tabela necessária e então na página /cadastro.php você poderá criar seu usuário
E na página /login.php acessar o sistema onde terá acesso ao /portfolio.php da empresa que é restrito a usuários cadastrados e logados.

### Changelog

* 2.0 Versão atualizando o site com PHP e SQL, cadastro e login.
* 1.0 Versão inicial somente com HTML e CSS.